import 'package:flutter/material.dart';
import 'package:lc3/Cafe/add_cafe.dart';
import 'package:lc3/admin/add_manager.dart';
import 'package:lc3/admin/admin_home_screen.dart';
import 'package:lc3/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:lc3/manager/add_cleaner.dart';
import 'package:lc3/utils/constants.dart';
import 'package:lc3/manager/manager_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: "AIzaSyAxlYf5Rr9cmQ_Tbxp7cMXyCpxvpjy8bhc",
        authDomain: "loocafe1.firebaseapp.com",
        projectId: "loocafe1",
        storageBucket: "loocafe1.appspot.com",
        messagingSenderId: "965128505371",
        appId: "1:965128505371:web:e6f9e3a45e3933bc3490be",
        measurementId: "G-PXW3XQRJHK"
    )
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override//852634
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context)=>MyLoginPage(),
        "/adminhomescreen": (context)=>const AdminHomeScreen(),
        "/manageradd": (context)=>const ManagerAdd(),
        "/managerhomescreen" : (context) => const Manager_page(),
        "/cleaneradd" : (context)=> const CleanerAdd(),
        "/cafeadd" : (context)=> const Cafeadd()
      },
      title: 'Flutter Demo',
      theme: ThemeData(primaryColor: COLOR_WHITE, accentColor: COLOR_DARK_BLUE, textTheme: TEXT_THEME_DEFAULT),
      initialRoute: "/",
    );
  }
}
