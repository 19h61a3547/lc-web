import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'missilians.dart';
class MyLoginPage extends StatelessWidget {
  var empid = TextEditingController();

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.amber, Colors.white,Colors.white, Colors.white,Colors.redAccent],
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Loo Cafe",
                style: TextStyle(
                    fontSize: 55,
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.amber,
                        width: 3
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          const Text(
                            "Enter Your Employee Id:",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic),
                          ),
                          Container(width: 200, child: TextField(cursorColor: Colors.redAccent,controller: empid,style: TextStyle(fontSize: 20.0),)),
                          const SizedBox(
                            height: 40,
                          ),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.amber),
                              onPressed: (){
                                submit(context,
                                []);
                                },
                              child: Text("Submit"))
                        ],
                      )))
            ],
          ),
        ),
      ),
    );


  }

  Future<void> submit(BuildContext context,List l) async {
    var s="";
    var collection = FirebaseFirestore.instance.collection('LC');
    var docSnapshot = await collection.doc(empid.text.toString().substring(2,4)).get();
    if (docSnapshot.exists) {
      Map<String, dynamic>? data = docSnapshot.data();
      s = data?['ids']; // <-- The value you want to retrieve.
      // Call setState if needed.
    }
    l=s.split(",");
    if(empid.text.toString()=="LCAD2247"){
      Navigator.popAndPushNamed(context, "/adminhomescreen");
      //Navigator.push(context, MaterialPageRoute(builder: (context)=> const MGPage()));
    }else if ( l.contains(empid.text.toString().substring(4))) {
      Navigator.popAndPushNamed(context, '/managerhomescreen',arguments: ScreenArguments(
          empid.text.toString().substring(4)
      ));
    }
  }


}