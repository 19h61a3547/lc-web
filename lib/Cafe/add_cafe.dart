import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../missilians.dart';

class Cafeadd extends StatefulWidget {
  const Cafeadd({Key? key}) : super(key: key);

  @override
  _CafeaddState createState() => _CafeaddState();
}

class _CafeaddState extends State<Cafeadd> {
  final lat = TextEditingController();
  final log = TextEditingController();
  final id = TextEditingController();
  static const snackBar = SnackBar(
      content: Text('Enter Details'),);


  @override
  Widget build(BuildContext context) {

    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;

    return Scaffold(
      appBar: AppBar(title:Text('Manager'),centerTitle: true,titleTextStyle: TextStyle(
          fontSize: 20.0,
          color: Colors.white

      ),),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 30.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Add Cafe',style: TextStyle(
                  fontSize: 35.0
              ),)
            ],
          ),
          SizedBox(height: 30.0,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Latitude',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                keyboardType: TextInputType.text,
                controller: lat,
              ))
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Longitude',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                controller: log,
              ))
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('ID',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                controller: id,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){
                if(lat.text.isEmpty || log.text.isEmpty || id.text.isEmpty){
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
                else{
                  register(args.id);
                  Navigator.pop(context);
                }
              }, child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Register',style: TextStyle(fontSize: 30.0),),
              ))
            ],
          ),
        ],
      ),
    );
  }

  void register(String mgid)async{
    var s="";
    var collection = FirebaseFirestore.instance.collection('LC');
    var docSnapshot = await collection.doc("MG").get();
    if (docSnapshot.exists) {
      Map<String, dynamic>? data = docSnapshot.data();
      s = data?['ids']; // <-- The value you want to retrieve.
      // Call setState if needed.
    }
    FirebaseFirestore.instance.collection('managers').doc(mgid).collection("cafes").doc(id.text.toString()).set({
      'name' : lat.text.toString(),
      'team' : log.text.toString(),
      'id' : id.text.toString()
    });
    FirebaseFirestore.instance.collection("LC").doc("CAF").set({
      "ids": s+","+id.text.toString()
    });
  }
}
