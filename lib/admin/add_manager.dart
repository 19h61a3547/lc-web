import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ManagerAdd extends StatefulWidget {
  const ManagerAdd({Key? key}) : super(key: key);

  @override
  _ManagerAddState createState() => _ManagerAddState();
}

class _ManagerAddState extends State<ManagerAdd> {

  final name = TextEditingController();
  final team = TextEditingController();
  final id = TextEditingController();
  static const snackBar = SnackBar(
    content: Text('Enter Details'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text('Administrator'),centerTitle: true,titleTextStyle: TextStyle(
          fontSize: 20.0,
          color: Colors.white

      ),),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 30.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Add Manager',style: TextStyle(
                fontSize: 35.0
              ),)
            ],
          ),
          SizedBox(height: 30.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text('Name',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                  fontSize: 30.0
                ),
                keyboardType: TextInputType.text,
                controller: name,
              ))
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Team',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                controller: team,
              ))
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('ID',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                keyboardType: TextInputType.number,
                controller: id,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){
                if(name.text.isEmpty || team.text.isEmpty || id.text.isEmpty){
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
                else{
                  register();
                  Navigator.pop(context);
                }
              }, child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Register',style: TextStyle(fontSize: 30.0),),
              ))
            ],
          ),
        ],
      ),
    );
  }

  void register()async{
    var s="";
    var collection = FirebaseFirestore.instance.collection('LC');
    var docSnapshot = await collection.doc("MG").get();
    if (docSnapshot.exists) {
      Map<String, dynamic>? data = docSnapshot.data();
      s = data?['ids']; // <-- The value you want to retrieve.
      // Call setState if needed.
    }
    FirebaseFirestore.instance.collection('managers').doc(id.text.toString()).set({
      'name' : name.text.toString(),
      'team' : team.text.toString(),
      'id' : id.text.toString()
    });
    FirebaseFirestore.instance.collection("LC").doc("MG").set({
      "ids": s+","+id.text.toString()
    });
  }
}

