import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lc3/missilians.dart';


class AdminHomeScreen extends StatefulWidget {
  const AdminHomeScreen({Key? key}) : super(key: key);

  @override
  _AdminHomeScreenState createState() => _AdminHomeScreenState();
}

class _AdminHomeScreenState extends State<AdminHomeScreen> {

  final _firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _firestore.collection("managers").snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> managersSnapshot){
        if (managersSnapshot.hasData){
          final managers = managersSnapshot.data?.docs;
          List<Widget> managerWidgets = [];
          for ( var m in managers!){
            managerWidgets.add(manager( name: m.get("name"), team: m.get("team"), icon:  const Icon(Icons.account_circle_rounded,size: 100.0,), color: 'l'));
          }
          if(managerWidgets.isNotEmpty){
            return HomeScreenAdmin(managerWidgets);
          }else{
            return HomeScreenAdmin(managerWidgets);
          }
        }else{
          return const HomeScreenAdmin([]);
        }
      },
    );
  }
}

class manager extends StatelessWidget {
  const manager({Key? key,required this.name, required this.team, required this.icon, required this.color}) : super(key: key);

  final String name,team,color;
  final Icon icon;

  @override
  Widget build(BuildContext context) {
    return OnHoverButton(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          decoration: BoxDecoration(
              color: color=='manager'? Colors.blue.withAlpha(40) : Colors.amber.withAlpha(120),
              borderRadius: BorderRadius.circular(20.0)
          ),
          height: 135,
          width: 125,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CircleAvatar(
                child: icon,
                radius: 50.0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                Text(name,style: TextStyle(color: Colors.black,fontSize: 35.0,fontFamily: 'font2'),),
                Text(team,style: TextStyle(color: Colors.black,fontSize: 15.0),)
              ],)
            ],
          ),
        ),
      ),
    );
  }
}

class HomeScreenAdmin extends StatelessWidget {
  const HomeScreenAdmin(this.l);

  final List<Widget> l;

  @override
  Widget build(BuildContext context) {

    final ThemeData themeData = Theme.of(context);

    if(l.isNotEmpty){
      return Scaffold(
        backgroundColor: Image.asset('assets/bg2.jpg').color,
        floatingActionButton: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, "/manageradd");
          },
          child: const SizedBox(height:50,width:50,child: Icon(Icons.add,size: 50,)),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue[200]
          ),
        ),

        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/bg.jpg'),
              fit: BoxFit.cover
            )
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20.0,top: 15.0),
                    child: Text('LOO CAFE',
                      style: TextStyle(
                        fontSize: 40.0,
                        fontFamily: 'Raleway',
                        color: Colors.black,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  GestureDetector(child : const Icon(Icons.logout,color: Colors.black,size: 30.0,), onTap: (){
                    Navigator.popAndPushNamed(context, "/");
                      },),
                ],
              ),
              Expanded(child: GridListExample(l)),
            ],
          ),
        )
      );
    }else{
      return Scaffold(
        floatingActionButton: Container(
          height: 50,
          width: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/manageradd");
              },
              child: const Center(child: Icon(Icons.add)),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blue[200]
              ),
            ),
          ),
        ),
        appBar: AppBar(
          title: const Text("Administrator",
          style: TextStyle(fontSize: 20.0),
          ),
          centerTitle: true,
          actions: [
            GestureDetector(child : const Icon(Icons.logout), onTap: (){
              Navigator.popAndPushNamed(context, "/");
            },),
            const SizedBox(width: 20,)
          ],
        ),
        body: const Center(
          child: Text("No Managers Present",style: TextStyle(color: Colors.grey,fontSize: 30),),
        ),
      );
    }
  }
}

class GridListExample extends StatefulWidget {
  GridListExample(this.l);
  final List l;
  @override
  State<GridListExample> createState() => _GridListExampleState(this.l);
}

class _GridListExampleState extends State<GridListExample> {

  final List l;

  _GridListExampleState(this.l);
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this would produce 2 rows.
      crossAxisCount: 5,
      scrollDirection: Axis.vertical,
      // Generate 100 Widgets that display their index in the List
      children: List.generate(l.length, (index) {
        return this.l[index];
      }),
    );
  }
}


