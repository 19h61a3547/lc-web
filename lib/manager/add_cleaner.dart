import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lc3/missilians.dart';

class CleanerAdd extends StatefulWidget {
  const CleanerAdd({Key? key}) : super(key: key);

  @override
  _CleanerAddState createState() => _CleanerAddState();
}

class _CleanerAddState extends State<CleanerAdd> {

  final name = TextEditingController();
  final team = TextEditingController();
  final id = TextEditingController();
  static const snackBar = SnackBar(
    content: Text('Enter Details'),
  );

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return Scaffold(
      appBar: AppBar(title:Text('Manager'),centerTitle: true,titleTextStyle: TextStyle(
          fontSize: 20.0,
          color: Colors.white

      ),),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 30.0,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Add Cleaner',style: TextStyle(
                  fontSize: 35.0
              ),)
            ],
          ),
          SizedBox(height: 30.0,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Name',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                keyboardType: TextInputType.text,
                controller: name,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Team',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                controller: team,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('ID',style: TextStyle(fontSize: 30.0),),
              SizedBox(width: 15.0,),
              Container(height: 30.0,width: 200.0,child: TextField(
                style: TextStyle(
                    fontSize: 30.0
                ),
                keyboardType: TextInputType.number,
                controller: id,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(onPressed: (){
                if(name.text.isEmpty || team.text.isEmpty || id.text.isEmpty){
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
                else{
                  register(args.id);
                  Navigator.pop(context);
                }
              }, child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Register',style: TextStyle(fontSize: 30.0),),
              ))
            ],
          ),
        ],
      ),
    );
  }
  void register(String mgid)async{
    var s="";
    var collection = FirebaseFirestore.instance.collection('LC');
    var docSnapshot = await collection.doc("CL").get();
    if (docSnapshot.exists) {
      Map<String, dynamic>? data = docSnapshot.data();
      s = data?['ids']; // <-- The value you want to retrieve.
      // Call setState if needed.
    }
    FirebaseFirestore.instance.collection('managers').doc(mgid).collection("cleaners").doc(id.text.toString()).set({
      'name' : name.text.toString(),
      'team' : team.text.toString(),
      'id' : id.text.toString()
    });
    FirebaseFirestore.instance.collection("LC").doc("CL").set({
      "ids": s+","+id.text.toString()
    });
  }
}
