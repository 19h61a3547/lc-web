import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lc3/manager/map.dart';
import '../missilians.dart';


class Manager_page extends StatefulWidget {
  const Manager_page({Key? key}) : super(key: key);
  @override
  _Manager_pageState createState() => _Manager_pageState();
}

class _Manager_pageState extends State<Manager_page> {

  final _firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {

    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return StreamBuilder(
      stream: _firestore.collection("managers").doc(args.id).collection("cleaners").snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> managersSnapshot){
        return StreamBuilder(
          stream: _firestore.collection("managers").doc(args.id).collection("cafes").snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> cafesSnapshot){
            if (managersSnapshot.hasData && cafesSnapshot.hasData){
              final managers = managersSnapshot.data?.docs;
              final cafes = cafesSnapshot.data?.docs;
              List<Widget> managerWidgets = [];
              List<Widget> cafesWidgets = [];
              for ( var m in managers!){
                managerWidgets.add(Cleaner( name: m.get("name"), team: m.get("team"), icon:  const Icon(Icons.account_circle_rounded,size: 100.0,), color: 'l'));
              }
              for ( var m in cafes!){
                cafesWidgets.add(Cafe( name: m.get("name"),team:m.get('id'), icon:  const Icon(Icons.apartment,size: 70.0,), color: 'l'));
              }
              if(managerWidgets.isNotEmpty){
                return HomeScreenmanager(managerWidgets,args.id,cafesWidgets);
              }else{
                return HomeScreenmanager(managerWidgets,args.id,cafesWidgets);
              }
            }else{
              return const HomeScreenmanager([],"",[]);
            }
          },
        );
      },
    );
  }
}

class Cleaner extends StatelessWidget {
  const Cleaner({Key? key,required this.name, required this.team, required this.icon, required this.color}) : super(key: key);

  final String name,team,color;
  final Icon icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            color: color=='manager'? Colors.blue.withAlpha(40) : Colors.amber.withAlpha(120),
            borderRadius: BorderRadius.circular(20.0)
        ),
        height: 135,
        width: 125,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            CircleAvatar(
              child: icon,
              radius: 50.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(name,style: TextStyle(color: Colors.black,fontSize: 35.0,fontFamily: 'font2'),),
                Text(team,style: TextStyle(color: Colors.black,fontSize: 15.0),)
              ],)
          ],
        ),
      ),
    );
  }
}

class Cafe extends StatefulWidget {
  const Cafe({Key? key,required this.name, required this.team, required this.icon, required this.color}) : super(key: key);

  final String name,team,color;
  final Icon icon;

  @override
  State<Cafe> createState() => _CafeState();
}

class _CafeState extends State<Cafe> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: (){
          setState(() {
            MapUtils.openmap('origin', 'destination');
          });
        },
        child: Container(
          decoration: BoxDecoration(
              color: widget.color=='manager'? Colors.blue.withAlpha(40) : Colors.amber.withAlpha(120),
              borderRadius: BorderRadius.circular(20.0)
          ),
          height: 135,
          width: 125,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CircleAvatar(
                child: widget.icon,
                radius: 50.0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(widget.team,style: TextStyle(color: Colors.black,fontSize: 35.0,fontFamily: 'font2'),),
                ],)
            ],
          ),
        ),
      ),
    );
  }
}


class HomeScreenmanager extends StatelessWidget {
  const HomeScreenmanager(this.l,this.id,this.cafe);

  final List<Widget> l,cafe;
  final String id;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    if(l.isNotEmpty && cafe.isNotEmpty){
      return Scaffold(
          backgroundColor: Image.asset('assets/bg.jpg').color,
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/cleaneradd",
                arguments: ScreenArguments(
                  id
                ));
              },
              child: const SizedBox(height:50,width:50,child: Icon(Icons.person_add,size: 50,)),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blue[200]
              ),
            ),
            SizedBox(width: 10,),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/cafeadd",
                    arguments: ScreenArguments(
                        id
                    ));
              },
              child: const SizedBox(height:50,width:50,child: Icon(Icons.apartment,size: 50,)),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blue[200]
              ),
            ),
          ],
        ),
        // appBar: AppBar(
        //   titleTextStyle: TextStyle(
        //       fontSize: 20.0,
        //       color: Colors.white
        //
        //   ),
        //   title: const Text("Manager"),
        //   centerTitle: true,
        //   actions: [
        //     GestureDetector(child : const Icon(Icons.logout), onTap: (){
        //       Navigator.popAndPushNamed(context, "/");
        //     },),
        //     const SizedBox(width: 20,)
        //   ],
        // ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/bg.jpg'),
                  fit: BoxFit.cover
              )
          ),
          child: Column(
            children:[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20.0,top: 15.0),
                    child: Text('LOO CAFE',
                      style: TextStyle(
                          fontSize: 40.0,
                          fontFamily: 'Raleway',
                          color: Colors.black,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  GestureDetector(child : const Icon(Icons.logout,color: Colors.black,size: 30.0,), onTap: (){
                    Navigator.popAndPushNamed(context, "/");
                  },),
                ],
              ),
              Expanded(child: GridListExample(cafe)),
              Expanded(child: GridListExample(l)),
            ],
          ),
        ),
      );
    }else if(l.isNotEmpty && cafe.isEmpty) {
      return Scaffold(
          backgroundColor: Image.asset('assets/bg.jpg').color,
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/cleaneradd",
                      arguments: ScreenArguments(
                          id
                      ));
                },
                child: const SizedBox(height:50,width:50,child: Icon(Icons.person_add,size: 50,)),
                style: ElevatedButton.styleFrom(
                    primary: Colors.blue[200]
                ),
              ),
              SizedBox(width: 10,),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/cafeadd",
                      arguments: ScreenArguments(
                          id
                      ));
                },
                child: const SizedBox(height:50,width:50,child: Icon(Icons.apartment,size: 50,)),
                style: ElevatedButton.styleFrom(
                    primary: Colors.blue[200]
                ),
              ),
            ],
          ),
          // appBar: AppBar(
          //   title: const Text("Manager"),
          //   centerTitle: true,
          //   actions: [
          //     GestureDetector(child : const Icon(Icons.logout), onTap: (){
          //       Navigator.popAndPushNamed(context, "/");
          //     },),
          //     const SizedBox(width: 20,)
          //   ],
          // ),
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/bg.jpg'),
                    fit: BoxFit.cover
                )
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20.0,top: 15.0),
                      child: Text('LOO CAFE',
                        style: TextStyle(
                            fontSize: 40.0,
                            fontFamily: 'Raleway',
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    GestureDetector(child : const Icon(Icons.logout,color: Colors.black,size: 30.0,), onTap: (){
                      Navigator.popAndPushNamed(context, "/");
                    },),
                  ],
                ),
                Text('Cafes',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),),
                Text('No Cafes',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),),
                Text('Cleaners',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                ),
                Expanded(child: GridListExample(l)),
              ],
            ),
          ),
      );
    }else if(l.isEmpty && cafe.isNotEmpty){
      return Scaffold(
        backgroundColor: Image.asset('assets/bg.jpg').color,
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/cleaneradd",
                      arguments: ScreenArguments(
                          id
                      ));
                },
                child: const SizedBox(height:50,width:50,child: Icon(Icons.person_add,size: 50,)),
                style: ElevatedButton.styleFrom(
                    primary: Colors.blue[200]
                ),
              ),
              SizedBox(width: 10,),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/cafeadd",
                      arguments: ScreenArguments(
                          id
                      ));
                },
                child: const SizedBox(height:50,width:50,child: Icon(Icons.apartment,size: 50,)),
                style: ElevatedButton.styleFrom(
                    primary: Colors.blue[200]
                ),
              ),
            ],
          ),
          // appBar: AppBar(
          //   title: const Text("Manager"),
          //   centerTitle: true,
          //   actions: [
          //     GestureDetector(child : const Icon(Icons.logout), onTap: (){
          //       Navigator.popAndPushNamed(context, "/");
          //     },),
          //     const SizedBox(width: 20,)
          //   ],
          // ),
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/bg.jpg'),
                    fit: BoxFit.cover
                )
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20.0,top: 15.0),
                      child: Text('LOO CAFE',
                        style: TextStyle(
                            fontSize: 40.0,
                            fontFamily: 'Raleway',
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    GestureDetector(child : const Icon(Icons.logout,color: Colors.black,size: 30.0,), onTap: (){
                      Navigator.popAndPushNamed(context, "/");
                    },),
                  ],
                ),
                Text('Cleaners',
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),),
                Text('No Cleaners',style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                ),),
                Text('Cafes',style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                ),),
                Expanded(child: GridListExample(cafe)),
              ],
            ),
          ),
      );
    }else{
      return Scaffold(
        backgroundColor: Image.asset('assets/bg.jpg').color,
        floatingActionButton: Container(
          height: 50,
          width: 50,
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/cleaneradd",
                    arguments: ScreenArguments(
                        id
                    ));
              },
              child: const Center(child: Icon(Icons.add)),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blue[200]
              ),
            ),
          ),
        ),
        // appBar: AppBar(
        //   title: const Text("Manager"),
        //   centerTitle: true,
        //   actions: [
        //     GestureDetector(child : const Icon(Icons.logout), onTap: (){
        //       Navigator.popAndPushNamed(context, "/");
        //     },),
        //     const SizedBox(width: 20,)
        //   ],
        // ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/bg.jpg'),
                  fit: BoxFit.cover
              )
          ),
          child: const Center(
            child: Text("No Cleaners Present",style: TextStyle(color: Colors.grey,fontSize: 30),),
          ),
        ),
      );
    }
  }
}

class GridListExample extends StatefulWidget {
  GridListExample(this.l);
  final List l;
  @override
  State<GridListExample> createState() => _GridListExampleState(this.l);
}

class _GridListExampleState extends State<GridListExample> {

  final List l;

  _GridListExampleState(this.l);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this would produce 2 rows.
      crossAxisCount: 5,
      scrollDirection: Axis.vertical,
      // Generate 100 Widgets that display their index in the List
      children: List.generate(l.length, (index) {
        return this.l[index];
      }),
    );
  }
}
